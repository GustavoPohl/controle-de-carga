import { Component } from '@angular/core';
import { AutenticacaoService } from '../service/jwt/autenticacao.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {

  constructor(
    public autenticacao: AutenticacaoService
  ){  }
}
