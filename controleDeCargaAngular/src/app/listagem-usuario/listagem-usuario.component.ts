import { Component } from '@angular/core';
import { UsuarioService } from '../service/data/usuario.service';
import { Router } from '@angular/router';
import { Usuario } from '../login/login.component';

@Component({
  selector: 'app-listagem-usuario',
  templateUrl: './listagem-usuario.component.html',
  styleUrls: ['./listagem-usuario.component.css']
})
export class ListagemUsuarioComponent {

  usuarios: Usuario [] | undefined;
  mensagem: string = '';

  constructor(
    private usuarioService: UsuarioService,
    private router : Router
  ) { }

  ngOnInit(){
    this.getAll();
  }

  getAll(){
    this.usuarioService.getAll().subscribe(
      (response: Usuario[]) =>{
        this.usuarios = response;
      }
    )
  }

  deletarUsuario(id: number){
    this.usuarioService.deleteUsuario(id).subscribe(
      (reponse: any) => {
        this.mensagem = 'Usuário Deletado';
        this.getAll();
      }
    )
  }

  updateUsuario(id: number){
    this.router.navigate(['registrarUsuario', id]);
  }

  insert(){
    this.router.navigate(['registrarUsuario', -1]);
  }
}
