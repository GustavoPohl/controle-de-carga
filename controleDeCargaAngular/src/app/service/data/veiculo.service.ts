import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Veiculo } from 'src/app/veiculo/veiculo.component';

@Injectable({
  providedIn: 'root'
})
export class VeiculoService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(){
    return this.http.get<Veiculo[]>(`http://localhost:8080/veiculo`)
  }

  delete(id: number){
    return this.http.delete(`http://localhost:8080/veiculo/delete/${id}`);
  }

  findById(id: number){
    return this.http.get<Veiculo>(`http://localhost:8080/veiculo/findById/${id}`);
  }

  update(id: number, veiculo: Veiculo){
    return this.http.put(`http://localhost:8080/veiculo/update/${id}`, veiculo);
  }

  insert(veiculo: Veiculo){
    return this.http.post(`http://localhost:8080/veiculo/insert`, veiculo);
  }
}
