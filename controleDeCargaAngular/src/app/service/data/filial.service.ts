import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Filial } from 'src/app/filial/filial.component';

@Injectable({
  providedIn: 'root'
})
export class FilialService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(){
    return this.http.get<Filial[]>(`http://localhost:8080/filial`)
  }

  delete(id: number){
    return this.http.delete(`http://localhost:8080/filial/delete/${id}`);
  }

  findById(id: number){
    return this.http.get<Filial>(`http://localhost:8080/filial/findById/${id}`);
  }

  update(id: number, filial: Filial){
    return this.http.put(`http://localhost:8080/filial/update/${id}`, filial);
  }

  insert(filial: Filial){
    return this.http.post(`http://localhost:8080/filial/insert`, filial);
  }
}
