import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from 'src/app/registrar-usuario/registrar-usuario.component';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(){
    return this.http.get<Usuario[]>(`http://localhost:8080/usuarios`)
  }

  deleteUsuario(id: number){
    return this.http.delete(`http://localhost:8080/usuario/delete/${id}`);
  }

  findByIdUsuario(id: number){
    return this.http.get<Usuario>(`http://localhost:8080/usuario/findById/${id}`);
  }

  updateUsuario(id: number, usuario: Usuario){
    return this.http.put(`http://localhost:8080/usuario/update/${id}`, usuario);
  }

  insertUsuario(usuario: Usuario){
    return this.http.post(`http://localhost:8080/usuario/insert`, usuario);
  }
}
