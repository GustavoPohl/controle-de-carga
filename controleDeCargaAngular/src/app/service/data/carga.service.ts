import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Carga } from 'src/app/registro-carga/registro-carga.component';

@Injectable({
  providedIn: 'root'
})
export class CargaService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(){
    return this.http.get<Carga[]>(`http://localhost:8080/carga`)
  }

  deleteCarga(id: number){
    return this.http.delete(`http://localhost:8080/carga/delete/${id}`);
  }

  findByIdCarga(id: number){
    return this.http.get<Carga>(`http://localhost:8080/carga/findById/${id}`);
  }

  updateCarga(id: number, carga: Carga){
    return this.http.put(`http://localhost:8080/carga/update/${id}`, carga);
  }

  insertCarga(carga: Carga){
    return this.http.post(`http://localhost:8080/carga/insert`, carga);
  }
}
