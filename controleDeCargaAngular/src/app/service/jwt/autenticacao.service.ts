import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AutenticacaoService {

  constructor(
    private http: HttpClient
  ) { }

  executarAutenticacao(username: string, password: string){
    return this.http.post<any>(`http://localhost:8080/authenticate`, {
      username,
      password
    }).pipe(
      map(
        (data: any) => {
          sessionStorage.setItem('token', `Bearer ${data.token}`);
          return data;
        }
      )
    )
  }

  isUsuarioLogado(){
    let user = sessionStorage.getItem('token');
    
    return !(user === null);
  }

  getUsuarioLogadoToken(){
    return sessionStorage.getItem('token');
  }

  logout(){
    sessionStorage.removeItem('token');
  }
}
