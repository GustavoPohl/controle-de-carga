import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AutenticacaoService } from '../jwt/autenticacao.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor(
    private autenticacao : AutenticacaoService
  ) { }
  
  intercept(request: HttpRequest<any>, next: HttpHandler){
    let basicAuthHeaderString = this.autenticacao.getUsuarioLogadoToken();
    if(basicAuthHeaderString){
      request = request.clone({
        setHeaders : {
          Authorization : basicAuthHeaderString
        }
      })
    }

    return next.handle(request);
  }
}
