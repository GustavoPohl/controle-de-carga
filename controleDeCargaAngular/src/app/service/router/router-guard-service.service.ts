import { Injectable } from '@angular/core';
import { AutenticacaoService } from '../jwt/autenticacao.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterGuardServiceService implements CanActivate{

  constructor(
    private router: Router,
    public autenticacao: AutenticacaoService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if(this.autenticacao.isUsuarioLogado())
      return true;
      this.router.navigate(['login']);
    
    return false;
  }
}
