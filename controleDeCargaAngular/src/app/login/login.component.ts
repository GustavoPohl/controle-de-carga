import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticacaoService } from '../service/jwt/autenticacao.service';

export class Usuario {
  constructor(
    public idUsuario : number,
    public nome : string,
    public email : string,
    public senha : string,
    public cpf : string,
    public isAdm : boolean
  ){}
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  username = '';
  password = '';
  mensagemErro = '';
  loginInvalido = false;

  constructor(
    public router: Router,
    public jwtAutenticacao : AutenticacaoService
  ){ }

  fazerLogin(){
    this.jwtAutenticacao.executarAutenticacao(this.username, this.password).subscribe(
      data => {
        this.loginInvalido = false;
        this.router.navigate(['dashboard']);
      },
      error =>{
        this.mensagemErro = 'Senha inválida';
        this.loginInvalido = true;
      }
    )
  }

}
