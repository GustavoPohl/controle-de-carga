import { Component } from '@angular/core';
import { FilialService } from '../service/data/filial.service';
import { ActivatedRoute, Router } from '@angular/router';

export class Filial {
  constructor(
    public idFilial : number,
    public nomeFilial : string,
    public telefone : string,
    public cnpj : string,
    public endereco : string,
    public cidade : string,
    public cep : string
  ){}
}

@Component({
  selector: 'app-filial',
  templateUrl: './filial.component.html',
  styleUrls: ['./filial.component.css']
})
export class FilialComponent {

  id: number = 0;
  filial: Filial = {
    idFilial : -1,
    nomeFilial : '',
    telefone : '',
    cnpj : '',
    endereco : '',
    cidade : '',
    cep : ''
  }

  constructor (
    private filialService: FilialService,
    private route : ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(){
    this.id = this.route.snapshot.params['id'];
    if(this.id != -1){
      this.filialService.findById(this.id).subscribe(
        data => this.filial = data
      )
    }
  }

  save(){
    if(this.id === -1){
      this.filialService.insert(this.filial).subscribe(
        data => {
          this.router.navigate(['listagemFilial'])
        }
      )
    } else{
      this.filialService.update(this.id, this.filial).subscribe(
        data => {
          this.router.navigate(['listagemFilial'])
        }
      )
    }
  }
}
