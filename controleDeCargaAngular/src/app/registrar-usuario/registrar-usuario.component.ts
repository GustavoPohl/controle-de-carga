import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../service/data/usuario.service';

export class Usuario {
  constructor(
    public idUsuario : number,
    public nome : string,
    public email : string,
    public senha : string,
    public cpf : string,
    public isAdm : boolean
  ){}
}

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './registrar-usuario.component.html',
  styleUrls: ['./registrar-usuario.component.css']
})
export class RegistrarUsuarioComponent {

  id: number = 0;
  usuario: Usuario = {
    idUsuario : -1,
    nome : '',
    email : '',
    senha : '',
    cpf : '',
    isAdm : false
  }

  constructor (
    private usuarioService: UsuarioService,
    private route : ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(){
    this.id = this.route.snapshot.params['id'];
    if(this.id != -1){
      this.usuarioService.findByIdUsuario(this.id).subscribe(
        data => this.usuario = data
      )
    }
  }

  saveUsuario(){
    if(this.id === -1){
      this.usuarioService.insertUsuario(this.usuario).subscribe(
        data => {
          this.router.navigate(['listagemUsuario'])
        }
      )
    } else{
      this.usuarioService.updateUsuario(this.id, this.usuario).subscribe(
        data => {
          this.router.navigate(['listagemUsuario'])
        }
      )
    }
  } 

}
