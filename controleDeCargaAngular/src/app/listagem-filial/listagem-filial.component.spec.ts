import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemFilialComponent } from './listagem-filial.component';

describe('ListagemFilialComponent', () => {
  let component: ListagemFilialComponent;
  let fixture: ComponentFixture<ListagemFilialComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListagemFilialComponent]
    });
    fixture = TestBed.createComponent(ListagemFilialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
