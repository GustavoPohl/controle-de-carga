import { Component } from '@angular/core';
import { Filial } from '../filial/filial.component';
import { FilialService } from '../service/data/filial.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listagem-filial',
  templateUrl: './listagem-filial.component.html',
  styleUrls: ['./listagem-filial.component.css']
})
export class ListagemFilialComponent {

  filiais: Filial [] | undefined;
  mensagem: string = '';

  constructor(
    private filialService: FilialService,
    private router : Router
  ) { }

  ngOnInit(){
    this.getAll();
  }

  getAll(){
    this.filialService.getAll().subscribe(
      (response: Filial[]) =>{
        this.filiais = response;
      }
    )
  }

  deletar(id: number){
    this.filialService.delete(id).subscribe(
      (reponse: any) => {
        this.mensagem = 'Filial Deletada';
        this.getAll();
      }
    )
  }

  update(id: number){
    this.router.navigate(['registrarFilial', id]);
  }

  insert(){
    this.router.navigate(['registrarFilial', -1]);
  }
}
