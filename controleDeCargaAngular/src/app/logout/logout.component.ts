import { Component } from '@angular/core';
import { AutenticacaoService } from '../service/jwt/autenticacao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {

  constructor(
    public autenticacao : AutenticacaoService,
    public router: Router
  ) { }

  ngOnInit(){
    this.autenticacao.logout();
    this.router.navigate(['login']);
  }

}
