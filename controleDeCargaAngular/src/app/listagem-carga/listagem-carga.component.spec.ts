import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemCargaComponent } from './listagem-carga.component';

describe('ListagemCargaComponent', () => {
  let component: ListagemCargaComponent;
  let fixture: ComponentFixture<ListagemCargaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListagemCargaComponent]
    });
    fixture = TestBed.createComponent(ListagemCargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
