import { Component } from '@angular/core';
import { Carga } from '../registro-carga/registro-carga.component';
import { CargaService } from '../service/data/carga.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listagem-carga',
  templateUrl: './listagem-carga.component.html',
  styleUrls: ['./listagem-carga.component.css']
})
export class ListagemCargaComponent {

  cargas: Carga [] | undefined;
  mensagem: string = '';

  constructor(
    private cargaService: CargaService,
    private router : Router
  ) { }

  ngOnInit(){
    this.getAll();
  }

  getAll(){
    this.cargaService.getAll().subscribe(
      (response: Carga[]) =>{
        this.cargas = response;
      }
    )
  }

  deletarCarga(id: number){
    this.cargaService.deleteCarga(id).subscribe(
      (reponse: any) => {
        this.mensagem = 'Carga Deletada';
        this.getAll();
      }
    )
  }

  update(id: number){
    this.router.navigate(['registrarCarga', id]);
  }

  insert(){
    this.router.navigate(['registrarCarga', -1]);
  }
}
