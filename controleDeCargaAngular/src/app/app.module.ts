import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { RegistrarUsuarioComponent } from './registrar-usuario/registrar-usuario.component';
import { MenuComponent } from './menu/menu.component';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ListagemUsuarioComponent } from './listagem-usuario/listagem-usuario.component';
import { PaginaErrorComponent } from './pagina-error/pagina-error.component';
import { InterceptorService } from './service/http/interceptor.service';
import { LogoutComponent } from './logout/logout.component';
import { RegistroCargaComponent } from './registro-carga/registro-carga.component';
import { ListagemCargaComponent } from './listagem-carga/listagem-carga.component';
import { ListagemFilialComponent } from './listagem-filial/listagem-filial.component';
import { FilialComponent } from './filial/filial.component';
import { VeiculoComponent } from './veiculo/veiculo.component';
import { ListagemVeiculoComponent } from './listagem-veiculo/listagem-veiculo.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PaginaInicialComponent,
    RegistrarUsuarioComponent,
    MenuComponent,
    ListagemUsuarioComponent,
    PaginaErrorComponent,
    LogoutComponent,
    RegistroCargaComponent,
    ListagemCargaComponent,
    FilialComponent,
    ListagemFilialComponent,
    VeiculoComponent,
    ListagemVeiculoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
