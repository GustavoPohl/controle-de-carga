import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrarUsuarioComponent } from './registrar-usuario/registrar-usuario.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { ListagemUsuarioComponent } from './listagem-usuario/listagem-usuario.component';
import { PaginaErrorComponent } from './pagina-error/pagina-error.component';
import { RouterGuardServiceService } from './service/router/router-guard-service.service';
import { LogoutComponent } from './logout/logout.component';
import { RegistroCargaComponent } from './registro-carga/registro-carga.component';
import { ListagemCargaComponent } from './listagem-carga/listagem-carga.component';
import { ListagemFilialComponent } from './listagem-filial/listagem-filial.component';
import { FilialComponent } from './filial/filial.component';
import { ListagemVeiculoComponent } from './listagem-veiculo/listagem-veiculo.component';
import { VeiculoComponent } from './veiculo/veiculo.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'login', component :LoginComponent},
  { path: 'listagemUsuario', component :ListagemUsuarioComponent, canActivate:[RouterGuardServiceService]},
  { path: 'registrarUsuario/:id', component :RegistrarUsuarioComponent, canActivate:[RouterGuardServiceService]},
  { path: 'listagemCarga', component :ListagemCargaComponent, canActivate:[RouterGuardServiceService]},
  { path: 'registrarCarga/:id', component :RegistroCargaComponent, canActivate:[RouterGuardServiceService]},
  { path: 'listagemFilial', component :ListagemFilialComponent, canActivate:[RouterGuardServiceService]},
  { path: 'registrarFilial/:id', component :FilialComponent, canActivate:[RouterGuardServiceService]},
  { path: 'listagemVeiculo', component :ListagemVeiculoComponent, canActivate:[RouterGuardServiceService]},
  { path: 'registrarVeiculo/:id', component :VeiculoComponent, canActivate:[RouterGuardServiceService]},
  { path: 'dashboard', component :PaginaInicialComponent, canActivate:[RouterGuardServiceService]},
  { path: 'logout', component :LogoutComponent, canActivate:[RouterGuardServiceService]},
  { path: '**', component :PaginaErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
