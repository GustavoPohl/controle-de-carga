import { Component } from '@angular/core';
import { VeiculoService } from '../service/data/veiculo.service';
import { ActivatedRoute, Router } from '@angular/router';

export class Veiculo {
  constructor(
    public idVeiculo : number,
    public placaVeiculo : string,
    public modelo : string,
    public marca : string,
    public tipoVeiculo : string
  ){}
}

@Component({
  selector: 'app-veiculo',
  templateUrl: './veiculo.component.html',
  styleUrls: ['./veiculo.component.css']
})
export class VeiculoComponent {

  id: number = 0;
  veiculo: Veiculo = {
    idVeiculo : -1,
    placaVeiculo : '',
    modelo : '',
    marca : '',
    tipoVeiculo : ''
  }

  constructor (
    private veiculoService: VeiculoService,
    private route : ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(){
    this.id = this.route.snapshot.params['id'];
    if(this.id != -1){
      this.veiculoService.findById(this.id).subscribe(
        data => this.veiculo = data
      )
    }
  }

  save(){
    if(this.id === -1){
      this.veiculoService.insert(this.veiculo).subscribe(
        data => {
          this.router.navigate(['listagemVeiculo'])
        }
      )
    } else{
      this.veiculoService.update(this.id, this.veiculo).subscribe(
        data => {
          this.router.navigate(['listagemVeiculo'])
        }
      )
    }
  }
}
