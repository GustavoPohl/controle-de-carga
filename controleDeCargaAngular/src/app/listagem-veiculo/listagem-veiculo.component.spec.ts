import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemVeiculoComponent } from './listagem-veiculo.component';

describe('ListagemVeiculoComponent', () => {
  let component: ListagemVeiculoComponent;
  let fixture: ComponentFixture<ListagemVeiculoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListagemVeiculoComponent]
    });
    fixture = TestBed.createComponent(ListagemVeiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
