import { Component } from '@angular/core';
import { Veiculo } from '../veiculo/veiculo.component';
import { VeiculoService } from '../service/data/veiculo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listagem-veiculo',
  templateUrl: './listagem-veiculo.component.html',
  styleUrls: ['./listagem-veiculo.component.css']
})
export class ListagemVeiculoComponent {

  veiculos: Veiculo [] | undefined;
  mensagem: string = '';

  constructor(
    private veiculoService: VeiculoService,
    private router : Router
  ) { }

  ngOnInit(){
    this.getAll();
  }

  getAll(){
    this.veiculoService.getAll().subscribe(
      (response: Veiculo[]) =>{
        this.veiculos = response;
      }
    )
  }

  deletar(id: number){
    this.veiculoService.delete(id).subscribe(
      (reponse: any) => {
        this.mensagem = 'Veículo Deletado';
        this.getAll();
      }
    )
  }

  update(id: number){
    this.router.navigate(['registrarVeiculo', id]);
  }

  insert(){
    this.router.navigate(['registrarVeiculo', -1]);
  }
}
