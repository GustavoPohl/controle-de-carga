import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCargaComponent } from './registro-carga.component';

describe('RegistroCargaComponent', () => {
  let component: RegistroCargaComponent;
  let fixture: ComponentFixture<RegistroCargaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RegistroCargaComponent]
    });
    fixture = TestBed.createComponent(RegistroCargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
