import { Component } from '@angular/core';
import { CargaService } from '../service/data/carga.service';
import { ActivatedRoute, Router } from '@angular/router';

export class Carga {
  constructor(
    public idCarga : number,
    public nomeCliente : string,
    public cidadeEnvio : string,
    public cidadeDestino : string,
    public tipoEnvio : string,
    public descricaoProdutos : string
  ){}
}

@Component({
  selector: 'app-registro-carga',
  templateUrl: './registro-carga.component.html',
  styleUrls: ['./registro-carga.component.css']
})
export class RegistroCargaComponent {

  id: number = 0;
  carga: Carga = {
    idCarga : -1,
    nomeCliente : '',
    cidadeEnvio : '',
    cidadeDestino : '',
    tipoEnvio : '',
    descricaoProdutos : ''
  }

  constructor (
    private cargaService: CargaService,
    private route : ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(){
    this.id = this.route.snapshot.params['id'];
    if(this.id != -1){
      this.cargaService.findByIdCarga(this.id).subscribe(
        data => this.carga = data
      )
    }
  }

  saveCarga(){
    if(this.id === -1){
      this.cargaService.insertCarga(this.carga).subscribe(
        data => {
          this.router.navigate(['listagemCarga'])
        }
      )
    } else{
      this.cargaService.updateCarga(this.id, this.carga).subscribe(
        data => {
          this.router.navigate(['listagemCarga'])
        }
      )
    }
  }
}
