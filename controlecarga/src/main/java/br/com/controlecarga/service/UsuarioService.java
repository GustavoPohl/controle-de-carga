package br.com.controlecarga.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.controlecarga.eao.UsuarioEAO;
import br.com.controlecarga.model.Usuario;

@Service
public class UsuarioService {

	private UsuarioEAO usuarioEAO;

	public UsuarioService(UsuarioEAO usuarioEAO) {
		this.usuarioEAO = usuarioEAO;
	}

	public List<Usuario> getAlll() {
		return usuarioEAO.findAll();
	}
	
	public void deleteById(Long id) {
		usuarioEAO.deleteById(id);
	}
	
	public Usuario insertOrUpdate(Usuario usuario) {
		return usuarioEAO.save(usuario);
	}
	
	public Optional<Usuario> findById(Long id){
		return usuarioEAO.findById(id);
	}
}
