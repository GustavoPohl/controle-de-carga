package br.com.controlecarga.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.controlecarga.eao.VeiculoEAO;
import br.com.controlecarga.model.Veiculo;

@Service
public class VeiculoService {

	private VeiculoEAO veiculoEAO;

	public VeiculoService(VeiculoEAO veiculoEAO) {
		super();
		this.veiculoEAO = veiculoEAO;
	}
	
	public List<Veiculo> getAlll() {
		return veiculoEAO.findAll();
	}
	
	public void deleteById(Long id) {
		veiculoEAO.deleteById(id);
	}
	
	public Veiculo insertOrUpdate(Veiculo veiculo) {
		return veiculoEAO.save(veiculo);
	}
	
	public Optional<Veiculo> findById(Long id){
		return veiculoEAO.findById(id);
	}
}
