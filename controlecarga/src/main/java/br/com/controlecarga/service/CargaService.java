package br.com.controlecarga.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.controlecarga.eao.CargaEAO;
import br.com.controlecarga.model.Carga;

@Service
public class CargaService {

	private CargaEAO cargaEAO;

	public CargaService(CargaEAO cargaEAO) {
		this.cargaEAO = cargaEAO;
	}

	public List<Carga> getAlll() {
		return cargaEAO.findAll();
	}

	public void deleteById(Long id) {
		cargaEAO.deleteById(id);
	}

	public Carga insertOrUpdate(Carga carga) {
		return cargaEAO.save(carga);
	}

	public Optional<Carga> findByID(Long id) {
		return cargaEAO.findById(id);
	}

}
