package br.com.controlecarga.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.controlecarga.eao.FilialEAO;
import br.com.controlecarga.model.Filial;

@Service
public class FilialService {

	private FilialEAO filialEAO;

	public FilialService(FilialEAO filialEAO) {
		this.filialEAO = filialEAO;
	}
	
	public List<Filial> getAlll() {
		return filialEAO.findAll();
	}
	
	public void deleteById(Long id) {
		filialEAO.deleteById(id);
	}
	
	public Filial insertOrUpdate(Filial filial) {
		return filialEAO.save(filial);
	}
	
	public Optional<Filial> findById(Long id){
		return filialEAO.findById(id);
	}
	
}
