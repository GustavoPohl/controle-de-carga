package br.com.controlecarga.eao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.controlecarga.model.Carga;

@Repository
public interface CargaEAO extends JpaRepository<Carga, Long>{

}
