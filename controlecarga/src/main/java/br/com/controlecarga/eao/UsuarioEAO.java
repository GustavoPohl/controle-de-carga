package br.com.controlecarga.eao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.controlecarga.model.Usuario;

@Repository
public interface UsuarioEAO extends JpaRepository<Usuario, Long>{

}
