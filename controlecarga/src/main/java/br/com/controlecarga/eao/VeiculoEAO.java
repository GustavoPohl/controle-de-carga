package br.com.controlecarga.eao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.controlecarga.model.Veiculo;

@Repository
public interface VeiculoEAO extends JpaRepository<Veiculo, Long>{

}
