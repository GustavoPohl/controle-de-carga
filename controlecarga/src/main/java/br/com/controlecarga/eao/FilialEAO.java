package br.com.controlecarga.eao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.controlecarga.model.Filial;

@Repository
public interface FilialEAO extends JpaRepository<Filial, Long>{

}
