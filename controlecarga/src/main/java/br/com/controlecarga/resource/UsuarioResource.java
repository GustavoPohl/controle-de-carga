package br.com.controlecarga.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.controlecarga.model.Usuario;
import br.com.controlecarga.service.UsuarioService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UsuarioResource {
	
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/usuarios")
	public List<Usuario> getAll(){
		return usuarioService.getAlll();
	}
	
	@GetMapping("/usuario/findById/{id}")
	public Optional<Usuario> findById(@PathVariable Long id){
		return usuarioService.findById(id);
	}
	
	@DeleteMapping("/usuario/delete/{id}")
	public void deleteById(@PathVariable Long id) {
		usuarioService.deleteById(id);
	}
	
	@PostMapping("/usuario/insert")
	public ResponseEntity<Void> insert(@RequestBody Usuario usuario) {
		usuario.setIdUsuario(null);
		
		Usuario insertOrUpdate = usuarioService.insertOrUpdate(usuario);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(insertOrUpdate.getIdUsuario()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("/usuario/update/{id}")
	public ResponseEntity<Usuario> update(@PathVariable Long id, @RequestBody Usuario usuario) {
		Usuario insertOrUpdate = usuarioService.insertOrUpdate(usuario);
		
		return new ResponseEntity<Usuario>(insertOrUpdate, HttpStatus.OK);
	}
}
