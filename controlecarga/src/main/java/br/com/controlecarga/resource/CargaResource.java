package br.com.controlecarga.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.controlecarga.model.Carga;
import br.com.controlecarga.service.CargaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CargaResource {

	@Autowired
	private CargaService cargaService;
	
	@GetMapping("/carga")
	public List<Carga> getAll(){
		return cargaService.getAlll();
	}
	
	@GetMapping("/carga/findById/{id}")
	public Optional<Carga> findById(@PathVariable Long id){
		return cargaService.findByID(id);
	}
	
	@DeleteMapping("/carga/delete/{id}")
	public void deleteById(@PathVariable Long id) {
		cargaService.deleteById(id);
	}
	
	@PostMapping("/carga/insert")
	public ResponseEntity<Void> insert(@RequestBody Carga carga) {
		Carga insertOrUpdate = cargaService.insertOrUpdate(carga);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(insertOrUpdate.getIdCarga()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("/carga/update/{id}")
	public ResponseEntity<Carga> update(@PathVariable Long id, @RequestBody Carga carga) {
		Carga insertOrUpdate = cargaService.insertOrUpdate(carga);
		
		return new ResponseEntity<Carga>(insertOrUpdate, HttpStatus.OK);
	}
}
