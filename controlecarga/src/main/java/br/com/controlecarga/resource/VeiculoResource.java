package br.com.controlecarga.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.controlecarga.model.Veiculo;
import br.com.controlecarga.service.VeiculoService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class VeiculoResource {
	
	@Autowired
	private VeiculoService veiculoService;
	
	@GetMapping("/veiculo")
	public List<Veiculo> getAll(){
		return veiculoService.getAlll();
	}
	
	@GetMapping("/veiculo/findById/{id}")
	public Optional<Veiculo> findById(@PathVariable Long id){
		return veiculoService.findById(id);
	}
	
	@DeleteMapping("/veiculo/delete/{id}")
	public void deleteById(@PathVariable Long id) {
		veiculoService.deleteById(id);
	}
	
	@PostMapping("/veiculo/insert")
	public ResponseEntity<Void> insert(@RequestBody Veiculo veiculo) {
		veiculo.setIdVeiculo(null);
		
		Veiculo insertOrUpdate = veiculoService.insertOrUpdate(veiculo);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(insertOrUpdate.getIdVeiculo()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("/veiculo/update/{id}")
	public ResponseEntity<Veiculo> update(@PathVariable Long id, @RequestBody Veiculo veiculo) {
		Veiculo insertOrUpdate = veiculoService.insertOrUpdate(veiculo);
		
		return new ResponseEntity<Veiculo>(insertOrUpdate, HttpStatus.OK);
	}

}
