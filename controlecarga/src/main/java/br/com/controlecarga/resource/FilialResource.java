package br.com.controlecarga.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.controlecarga.model.Filial;
import br.com.controlecarga.service.FilialService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class FilialResource {
	
	@Autowired
	private FilialService filialService;
	
	@GetMapping("/filial")
	public List<Filial> getAll(){
		return filialService.getAlll();
	}
	
	@GetMapping("/filial/findById/{id}")
	public Optional<Filial> findById(@PathVariable Long id){
		return filialService.findById(id);
	}
	
	@DeleteMapping("/filial/delete/{id}")
	public void deleteById(@PathVariable Long id) {
		filialService.deleteById(id);
	}
	
	@PostMapping("/filial/insert")
	public ResponseEntity<Void> insert(@RequestBody Filial filial) {
		filial.setIdFilial(null);
		
		Filial insertOrUpdate = filialService.insertOrUpdate(filial);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(insertOrUpdate.getIdFilial()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("/filial/update/{id}")
	public ResponseEntity<Filial> update(@PathVariable Long id, @RequestBody Filial filial) {
		Filial insertOrUpdate = filialService.insertOrUpdate(filial);
		
		return new ResponseEntity<Filial>(insertOrUpdate, HttpStatus.OK);
	}

}
