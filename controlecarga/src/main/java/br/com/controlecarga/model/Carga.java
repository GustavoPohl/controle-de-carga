package br.com.controlecarga.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Carga {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long idCarga;

	private String nomeCliente;

	private String cidadeEnvio;

	private String cidadeDestino;

	private String tipoEnvio;

	private String descricaoProdutos;

	public Long getIdCarga() {
		return idCarga;
	}

	public void setIdCarga(Long idCarga) {
		this.idCarga = idCarga;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getCidadeEnvio() {
		return cidadeEnvio;
	}

	public void setCidadeEnvio(String cidadeEnvio) {
		this.cidadeEnvio = cidadeEnvio;
	}

	public String getCidadeDestino() {
		return cidadeDestino;
	}

	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}

	public String getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public String getDescricaoProdutos() {
		return descricaoProdutos;
	}

	public void setDescricaoProdutos(String descricaoProdutos) {
		this.descricaoProdutos = descricaoProdutos;
	}

}
