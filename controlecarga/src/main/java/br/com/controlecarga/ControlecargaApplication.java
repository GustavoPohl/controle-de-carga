package br.com.controlecarga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlecargaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlecargaApplication.class, args);
	}

}
